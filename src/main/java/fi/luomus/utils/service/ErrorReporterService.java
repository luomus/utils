package fi.luomus.utils.service;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ErrorReporterService {
    private final ErrorReportMailer errorReportMailer;

    public ErrorReporterService(ErrorReportMailer errorReportMailer) {
        this.errorReportMailer = errorReportMailer;
    }

    public void report(Throwable exception, HttpServletRequest req) {
        StringBuilder stackTraceBuilder = new StringBuilder();
        String requestUriScrubbed = req.getRequestURI() != null ? req.getRequestURI().split(";jsessionid")[0] : null;
        stackTraceBuilder.append("Request details\n")
                .append(String.format("Request URL: %s\n", requestUriScrubbed))
                .append(String.format("Request method: %s\n", req.getMethod()));
        if (req.getParameterMap().size() > 0) {
            stackTraceBuilder.append("Request params:\n");
            for (Map.Entry<String, String[]> entry : req.getParameterMap().entrySet()) {
                for (String value : entry.getValue()) {
                    stackTraceBuilder.append(String.format("%s => %s\n", entry.getKey(), value));
                }
            }
        }

        Set<Throwable> reportedExceptions = new HashSet<>();
        while (!reportedExceptions.contains(exception) && exception != null) {
            reportedExceptions.add(exception);
            StringWriter writer = new StringWriter();
            exception.printStackTrace(new PrintWriter(writer));
            stackTraceBuilder.append(writer.toString());
            stackTraceBuilder.append("\n==========\n");
            exception = exception.getCause();
        }

        String report = stackTraceBuilder.toString();
        errorReportMailer.mail(report);
    }
}
