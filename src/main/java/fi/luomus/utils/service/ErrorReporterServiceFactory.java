package fi.luomus.utils.service;

public class ErrorReporterServiceFactory {
    public static ErrorReporterService reporter(JavaMailErrorReportMailer.JavaMailErrorReportMailerConfiguration configuration) {
        return new ErrorReporterService(new JavaMailErrorReportMailer(configuration));
    }
}
