package fi.luomus.utils.service;

public interface ErrorReportMailer {
    void mail(String exception);
}
