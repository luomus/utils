package fi.luomus.utils.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class MessageRecord {

	private static final int DEFAULT_THRESHOLD_IN_HOURS_HOW_OFTEN_SAME_MESSAGE_IS_SEND = 7;
	private static final int DEFAULT_MAX_NUMBER_OF_EMAILS_IN_HOUR_TO_SEND = 3;
	private final Cache<String, Boolean> sentMessages;
	private final Cache<String, Integer> numberOfEmailsInHour;
	private final int maxNumberOfEmailsInHourToSend;

	public MessageRecord() {
		this(DEFAULT_THRESHOLD_IN_HOURS_HOW_OFTEN_SAME_MESSAGE_IS_SEND, DEFAULT_MAX_NUMBER_OF_EMAILS_IN_HOUR_TO_SEND); 
	}

	public MessageRecord(int howOftenSameMessageIsSendInHours, int maxNumberOfEmailsInHourToSend) {
		this.maxNumberOfEmailsInHourToSend = maxNumberOfEmailsInHourToSend;
		this.sentMessages = CacheBuilder.newBuilder().expireAfterWrite(howOftenSameMessageIsSendInHours, TimeUnit.HOURS).build();
		this.numberOfEmailsInHour = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.HOURS).build();
	}

	public void record(String message) {
		sentMessages.put(truncateMessage(message), true);
		increaseNumberOfMessagesSendThisHourByOne();
	}

	private String truncateMessage(String message) {
		try {
			if (message == null) return "";
			message = removeWhitespace(message);
			if (message.contains(":")) {
				return message.substring(0, message.indexOf(":") - 1);
			}
			if (message.length() < 30) {
				return message;
			}
			return message.substring(0, 29);
		} catch (Exception e) {
			return message;
		}
	}

	private void increaseNumberOfMessagesSendThisHourByOne() {
		numberOfEmailsInHour.put(thisHour(), numberThisHour() + 1);
	}

	private String thisHour() {
		Calendar k = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHH");
		return sdf.format(k.getTime());
	}

	private Integer numberThisHour() {
		Integer numberThisHour = numberOfEmailsInHour.getIfPresent(thisHour());
		if (numberThisHour == null) return 0;
		return numberThisHour;
	}

	public boolean shouldSend(String message) {
		return notSentWithinThreshold(message) && numberThisHour() < maxNumberOfEmailsInHourToSend;
	}

	private boolean notSentWithinThreshold(String message) {
		return sentMessages.getIfPresent(truncateMessage(message)) == null;
	}

	private static String removeWhitespace(String value) {
		return value.replaceAll("\\s", "");
	}

}
