package fi.luomus.utils.service;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.LoggerFactory;

public class JavaMailErrorReportMailer implements ErrorReportMailer {

	private final JavaMailErrorReportMailerConfiguration configuration;
	private final MessageRecord messageRecord = new MessageRecord();

	public JavaMailErrorReportMailer(JavaMailErrorReportMailerConfiguration configuration) {
		this.configuration = configuration;
	}

	@Override
	public void mail(String exception) {
		if (messageRecord.shouldSend(exception)) {
			mailActual(exception);
			messageRecord.record(exception);
		}
	}

	private void mailActual(String exception) {
		String from = "noreply@luomus.fi";

		Properties props = new Properties();
		props.put("mail.smtp.host", configuration.getHost());
		Session session = Session.getInstance(props);

		Message msg = new MimeMessage(session);

		try {
			msg.setFrom(new InternetAddress(from));
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		String[] recipients = configuration.getRecipients();
		InternetAddress[] addresses = new InternetAddress[recipients.length];
		int i = 0;
		for (String recipient : recipients) {
			try {
				addresses[i++] = new InternetAddress(recipient);
			} catch (AddressException e) {
				LoggerFactory.getLogger(this.getClass()).error("invalid exception reporter destination address " + recipient, e);
			}
		}
		try {
			msg.setRecipients(Message.RecipientType.TO, addresses);
			msg.setSubject(configuration.getSubject());
			msg.setSentDate(new Date());
			msg.setText(exception);
			Transport.send(msg);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public interface JavaMailErrorReportMailerConfiguration {
		String getHost();
		String getSubject();
		String[] getRecipients();
	}
}
