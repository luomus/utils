package fi.luomus.utils.exceptions;

/**
 * Used to designate exceptions that are the result of
 * not being able to contact an API due to a network or
 * service outage. Mainly used to reduce unnecessary
 * exception reporting.
 */
public class ApiException extends Exception {
	private static final long serialVersionUID = -4792380053982792762L;
	private final String source;
    private final String code;
    private final String details;

    private ApiException(ApiExceptionBuilder builder) {
        super(builder.getMessage());
        this.source = builder.source;
        this.code = builder.code;
        this.details = builder.details;
    }

    private ApiException(ApiExceptionBuilder builder, Throwable cause) {
        super(builder.getMessage(), cause);
        this.source = builder.source;
        this.code = builder.code;
        this.details = builder.details;
    }

    /**
     * Get the identifier of the system causing the exception
     * @return identifier
     */
    public String getSource() {
        return source;
    }

    /**
     * Get the error code related to the exception (e.g. "500" for internal server error for
     * an HTTP API call error)
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     * Get the free-form description of the error (error message, stack trace, etc)
     * @return
     */
    public String getDetails() {
        return details;
    }

    public static ApiExceptionBuilder builder() {
        return new ApiExceptionBuilder(null, null, null, null);
    }

    public static class ApiExceptionBuilder {
        public final String source;
        public final String code;
        public final String details;
        public final Throwable cause;

        private ApiExceptionBuilder(String source, String code, String details, Throwable cause) {
            this.source = source;
            this.code = code;
            this.details = details;
            this.cause = cause;
        }

        public ApiExceptionBuilder source(String source) {
            return new ApiExceptionBuilder(source, code, details, cause);
        }

        public ApiExceptionBuilder code(String code) {
            return new ApiExceptionBuilder(source, code, details, cause);
        }

        public ApiExceptionBuilder details(String details) {
            return new ApiExceptionBuilder(source, code, details, cause);
        }

        public ApiExceptionBuilder cause(Throwable cause) {
            return new ApiExceptionBuilder(source, code, details, cause);
        }

        public String getMessage() {
            return String.format("Received code %s from source %s, details:\n%s",
                    code,
                    source,
                    details);
        }

        public ApiException build() {
            if (cause != null) {
                return new ApiException(this, cause);
            }
			return new ApiException(this);
        }
    }
}
