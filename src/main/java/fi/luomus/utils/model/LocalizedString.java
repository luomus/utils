package fi.luomus.utils.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Used to represent human-readable plaintext strings
 * that are available in multiple languages. The key should
 * correspond to a ISO-639-1 two-letter language code (eg. "fi", "sv", "en")
 */
public class LocalizedString extends HashMap<String, String> {

	private static final long serialVersionUID = 914758079800001545L;

	public LocalizedString() {
	}

	public LocalizedString(Map<? extends String, ? extends String> m) {
		super(m);
	}

	public static LocalizedStringFactory localizedString() {
		return new LocalizedStringFactory();
	}

	/**
	 * Utility class for fluent creation
	 */
	public static class LocalizedStringFactory {
		private final HashMap<String, String> mappings = new HashMap<>();

		public LocalizedStringFactory with(String language, String text) {
			mappings.put(language, text);
			return this;
		}

		public LocalizedString get() {
			return new LocalizedString(mappings);
		}
	}
}
