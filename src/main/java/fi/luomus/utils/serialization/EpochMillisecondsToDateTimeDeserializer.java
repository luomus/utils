package fi.luomus.utils.serialization;

import java.io.IOException;

import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class EpochMillisecondsToDateTimeDeserializer extends JsonDeserializer<DateTime> {

	@Override
	public DateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
		return deserialize(jsonParser.readValueAs(Long.class));
	}

	public DateTime deserialize(long epoch) {
		if (epoch > 4000000000L) return new DateTime(epoch);
		return new DateTime(epoch*1000);
	}

}
