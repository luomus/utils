package fi.luomus.utils;

import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Installs slf4j JUL adapter upon application startup
 */
public class LoggingBridgeInstaller implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
        LoggerFactory.getLogger(LoggingBridgeInstaller.class).info("installed JUL logging adapter");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
