package fi.luomus.utils.preferences;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;

public class LuomusPreferencesImpl extends BaseLuomusPreferences {
    private final Preferences preferences;
    private final static String NOT_FOUND = "XXXNOTFOUNDXXX";
    private final Logger logger = LoggerFactory.getLogger(LuomusPreferencesImpl.class);

    public LuomusPreferencesImpl(Preferences preferences) {
        if (preferences == null) {
            throw new IllegalArgumentException();
        }
        this.preferences = preferences;
        logPreferences();
    }

    public LuomusPreferencesImpl(Class<?> baseClass) {
        preferences = Preferences.userNodeForPackage(baseClass);
        logPreferences();
    }

    public LuomusPreferencesImpl(Class<?> baseClass, String profile) {
        preferences = Preferences.userNodeForPackage(baseClass).node(profile);
        logPreferences();
    }

    private void logPreferences() {
        logger.info("Initialized Java Preferences backed LuomusPreferences, path: " + preferences.absolutePath());
        if (logger.isDebugEnabled()) {
            try {
                for (String key : preferences.keys()) {
                    String msg = String.format("%s -> %s", key, preferences.get(key, "???"));
                    logger.debug(msg);
                }
            } catch (BackingStoreException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public Optional<String> get(Preference preference) {
        String value = preferences.get(preference.key, NOT_FOUND);
        if (value.equals(NOT_FOUND)) {
            return Optional.absent();
        }
		return Optional.of(value);
    }

    @Override
    public Set<Preference> keys() {
        Set<Preference> result = new HashSet<>();
        try {
            for (String key : preferences.keys()) {
                result.add(new Preference(key));
            }
        } catch (BackingStoreException e) {
            throw new RuntimeException("error while getting backing preferences object keys", e);
        }

        return result;
    }

    @Override
    public synchronized void set(Preference preference, Object... newValues) {
        preferences.put(preference.key, Joiner.on(DELIMITER).join(newValues));
        try {
            preferences.sync();
        } catch (BackingStoreException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setList(Preference preference, List<? extends Object> values) {
        this.set(preference, values.toArray());
    }

    @Override
    public void remove(Preference preference) {
        preferences.remove(preference.key);
        try {
            preferences.sync();
        } catch (BackingStoreException e) {
            throw new RuntimeException(e);
        }
    }
}
