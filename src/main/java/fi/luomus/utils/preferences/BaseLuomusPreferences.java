package fi.luomus.utils.preferences;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Optional;
import com.google.common.base.Splitter;

public abstract class BaseLuomusPreferences implements LuomusPreferences {
    protected static final String DELIMITER = ";";

    @Override
    public List<String> getMulti(Preference preference) {
        Optional<String> optional = this.get(preference);
        if (optional.isPresent()) {
            return Splitter.on(DELIMITER).splitToList(optional.get());
        }
		return new ArrayList<>();
    }

    @Override
    public Optional<Long> getLong(Preference preference) {
        Optional<String> optional = this.get(preference);
        if (optional.isPresent()) {
            return Optional.of(Long.valueOf(optional.get()));
        }
		return Optional.absent();
    }

    @Override
    public Optional<Float> getFloat(Preference preference) {
        Optional<String> optional = this.get(preference);
        if (optional.isPresent()) {
            return Optional.of(Float.valueOf(optional.get()));
        }
		return Optional.absent();
    }

    @Override
    public Optional<Boolean> getBoolean(Preference preference) {
        Optional<String> optional = this.get(preference);
        if (optional.isPresent()) {
            return Optional.of(Boolean.valueOf(optional.get()));
        }
		return Optional.absent();
    }
}
