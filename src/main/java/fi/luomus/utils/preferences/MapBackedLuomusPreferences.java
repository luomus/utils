package fi.luomus.utils.preferences;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;

import java.util.*;

public class MapBackedLuomusPreferences extends BaseLuomusPreferences {
    private final Map<String, String> preferences = new HashMap<>();

    public MapBackedLuomusPreferences() {
    }

    public MapBackedLuomusPreferences(Map<String, Object> prefs) {
        for (String key : prefs.keySet()) {
            preferences.put(key, prefs.get(key).toString());
        }
    }

    @Override
    public Optional<String> get(Preference preference) {
        return Optional.fromNullable(preferences.get(preference.key));
    }

    @Override
    public void set(Preference preference, Object... newValues) {
        preferences.put(preference.key, Joiner.on(DELIMITER).join(newValues));
    }

    @Override
    public void setList(Preference preference, List<? extends Object> values) {
        this.set(preference, values.toArray());
    }

    @Override
    public void remove(Preference preference) {
        preferences.remove(preference.key);
    }

    @Override
    public Set<Preference> keys() {
        Set<Preference> result = new HashSet<>();
        for (String key : this.preferences.keySet() ) {
            result.add(new Preference(key));
        }

        return result;
    }
}
