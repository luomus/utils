package fi.luomus.utils.preferences;

public class Preference {
    public final String key;
    public final String description;

    public Preference(String key, String description) {
        this.key = key;
        this.description = description;
    }

    public Preference(String key) {
        this(key, null);
    }
}
