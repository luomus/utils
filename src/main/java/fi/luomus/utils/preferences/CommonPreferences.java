package fi.luomus.utils.preferences;

public class CommonPreferences {
    private CommonPreferences() {
    }

    public final static Preference DEV_MODE = new Preference("dev_mode");
    public final static Preference SECRET = new Preference("secret");
}
