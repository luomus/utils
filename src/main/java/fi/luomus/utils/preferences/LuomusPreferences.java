package fi.luomus.utils.preferences;

import com.google.common.base.Optional;

import java.util.List;
import java.util.Set;

public interface LuomusPreferences {
    Optional<String> get(Preference preference);
    List<String> getMulti(Preference preference);
    Optional<Long> getLong(Preference preference);
    Optional<Float> getFloat(Preference preference);
    Optional<Boolean> getBoolean(Preference preference);
    Set<Preference> keys();

    void set(Preference preference, Object... newValues);
    void setList(Preference preference, List<? extends Object> values);
    void remove(Preference preference);
}
