package fi.luomus.utils.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

public class ErrorReporterServiceTest {

    @Test
    public void testParamMap() {
        String key = UUID.randomUUID().toString();
        String firstValue = UUID.randomUUID().toString();
        String secondValue = UUID.randomUUID().toString();
        Map<String, String[]> paramMap = new HashMap<>();
        paramMap.put(key, new String[]{firstValue, secondValue});
        ErrorReportMailer mock = mock(ErrorReportMailer.class);
        ErrorReporterService errorReportService = new ErrorReporterService(mock);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameterMap()).thenReturn(paramMap);
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        errorReportService.report(new Exception(), request);
        verify(mock).mail(captor.capture());
        String report = captor.getValue();
        assertTrue(report.contains(key) && report.contains(firstValue) && report.contains(secondValue));
    }

    @Test
    public void testInnerLoopedExceptions() {
        ErrorReportMailer mock = mock(ErrorReportMailer.class);
        ErrorReporterService errorReportService = new ErrorReporterService(mock);
        Exception secondException = new Exception("bar456");
        Exception firstException = new Exception("foo123");
        firstException.initCause(secondException);
        secondException.initCause(firstException);
        HttpServletRequest request = mock(HttpServletRequest.class);
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        errorReportService.report(firstException, request);
        verify(mock).mail(captor.capture());
        String value = captor.getValue();
        assertTrue(value.contains("foo123") && value.contains("bar456"));
    }

    @Test
    public void testIgnoreJsession() {
        ErrorReportMailer mock = mock(ErrorReportMailer.class);
        ErrorReporterService errorReportService = new ErrorReporterService(mock);
        Exception exception = new Exception();
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/loydos/forms/sammakot-ja-matelijat;jsessionid=6126EA4910BB28C919F28390BE9A0DC0");
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        errorReportService.report(exception, request);
        verify(mock).mail(captor.capture());
        verify(mock, times(1)).mail(anyString());
        assertFalse(captor.getValue().contains(";jsessionid=6126EA4910BB28C919F28390BE9A0DC0"));
        assertTrue(captor.getValue().contains("loydos/forms/sammakot-ja-matelijat"));
    }
}