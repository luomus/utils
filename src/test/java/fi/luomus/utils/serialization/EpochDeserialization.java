package fi.luomus.utils.serialization;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EpochDeserialization {

	@Test
	public void testEpochDeserialization() {
		long millis = 1738057081000L;
		long seconds = 1738057081L;

		assertEquals("2025-01-28T11:38:01.000+02:00", new EpochMillisecondsToDateTimeDeserializer().deserialize(millis).toString());
		assertEquals("2025-01-28T11:38:01.000+02:00", new EpochMillisecondsToDateTimeDeserializer().deserialize(seconds).toString());

		assertEquals("2025-01-28T11:38:01.000+02:00", new EpochSecondsToDateTimeDeserializer().deserialize(millis).toString());
		assertEquals("2025-01-28T11:38:01.000+02:00", new EpochSecondsToDateTimeDeserializer().deserialize(seconds).toString());
	}

}