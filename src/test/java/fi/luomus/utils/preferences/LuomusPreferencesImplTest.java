package fi.luomus.utils.preferences;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.junit.Before;
import org.junit.Test;

public class LuomusPreferencesImplTest {
    private final Preference TEST_PREFERENCE = new Preference("test");
    private Preferences preferences;

    @Before
    public void cleanUp() throws BackingStoreException {
        preferences = Preferences.userNodeForPackage(LuomusPreferencesImplTest.class);
        preferences.clear();
        preferences.sync();

    }

    @Test
    public void testSingle() {
        LuomusPreferences luomusPreferences = new LuomusPreferencesImpl(preferences);
        assertFalse(luomusPreferences.get(TEST_PREFERENCE).isPresent());
        final String VALUE = "foobar";
        luomusPreferences.set(TEST_PREFERENCE, VALUE);
        assertEquals(VALUE, luomusPreferences.get(TEST_PREFERENCE).get());
    }

    @Test
    public void testMulti() {
        LuomusPreferences luomusPreferences = new LuomusPreferencesImpl(preferences);
        assertEquals(0, luomusPreferences.getMulti(TEST_PREFERENCE).size());
        final String[] VALUES = {"foo", "bar"};
        luomusPreferences.set(TEST_PREFERENCE, (Object[]) VALUES);
        assertEquals(Arrays.asList(VALUES), luomusPreferences.getMulti(TEST_PREFERENCE));
        luomusPreferences.setList(TEST_PREFERENCE, Arrays.asList(VALUES));
        assertEquals(Arrays.asList(VALUES), luomusPreferences.getMulti(TEST_PREFERENCE));
    }

    @Test
    public void testBoolean() {
        LuomusPreferences luomusPreferences = new LuomusPreferencesImpl(preferences);
        assertFalse(luomusPreferences.getBoolean(TEST_PREFERENCE).isPresent());
        final boolean VALUE = true;
        luomusPreferences.set(TEST_PREFERENCE, VALUE);
        assertTrue(luomusPreferences.getBoolean(TEST_PREFERENCE).get());
    }

    @Test
    public void testInt() {
        LuomusPreferences luomusPreferences = new LuomusPreferencesImpl(preferences);
        assertFalse(luomusPreferences.getLong(TEST_PREFERENCE).isPresent());
        final long VALUE = 1337L;
        luomusPreferences.set(TEST_PREFERENCE, VALUE);
        assertTrue(1337L == luomusPreferences.getLong(TEST_PREFERENCE).get());
    }

    @Test
    public void testFloat() {
        LuomusPreferences luomusPreferences = new LuomusPreferencesImpl(preferences);
        assertFalse(luomusPreferences.getLong(TEST_PREFERENCE).isPresent());
        final double VALUE = 1337.0;
        luomusPreferences.set(TEST_PREFERENCE, VALUE);
        assertTrue(Math.abs(1337.0 - luomusPreferences.getFloat(TEST_PREFERENCE).get()) < 0.0001);
    }

    @Test
    public void keys() {
        LuomusPreferences luomusPreferences = new LuomusPreferencesImpl(preferences);
        assertEquals(0, luomusPreferences.keys().size());
        for (int i = 0; i < 50; i++) {
            luomusPreferences.set(TEST_PREFERENCE, "value");
            luomusPreferences.set(new Preference("KEY_" + i), "value");
            assertEquals((i + 2), luomusPreferences.keys().size());
        }
    }

    @Test
    public void testRemove() {
        LuomusPreferences luomusPreferences = new LuomusPreferencesImpl(preferences);
        assertEquals(0, luomusPreferences.keys().size());
        for (int i = 0; i < 50; i++) {
            luomusPreferences.set(TEST_PREFERENCE, "value");
            luomusPreferences.set(new Preference("KEY_" + i), "value");
            assertEquals((i + 2), luomusPreferences.keys().size());
        }
        for (int i = 0; i < 50; i++) {
            luomusPreferences.remove(new Preference("KEY_" + i));
            assertEquals(51 - (i + 1), luomusPreferences.keys().size());
        }
    }

}